#include "sor.h"
#include <math.h>
#include <stdio.h>

void sor( double omg, double dx, double dy, int imax, int jmax, double **P, double **RS, double *res, int **flags) {
  int i,j,N;
  double rloc;

  /* set boundary values */
  for(i = 1; i <= imax; i++) {
    P[i][0] = P[i][1];
    P[i][jmax+1] = P[i][jmax];
  }
  for(j = 1; j <= jmax; j++) {
    P[0][j] = P[1][j];
    P[imax+1][j] = P[imax][j];
  }

  /* set boundary condition in inside obstacles */
  int B_O, B_W, B_S, B_N, f, B_NO, B_NW, B_SO, B_SW;
    for(i=1; i<=imax; i++)
    	for(j=1; j<=jmax; j++){
    		f = flags[i][j];
    		if (f%2==0){
        		B_O = (f >> 8)&1;
        		B_W = (f >> 7)&1;
        		B_S = (f >> 6)&1;
        		B_N = (f >> 5)&1;
        		B_NO = B_N & B_O;
        		B_NW = B_N & B_W;
        		B_SO = B_S & B_O;
        		B_SW = B_S & B_W;
                P[i][j] = B_N*P[i][j+1] + B_S*P[i][j-1] + B_W*P[i-1][j] + B_O*P[i+1][j]
                        - 0.5*B_NO*(P[i][j+1]+P[i+1][j]) - 0.5*B_NW*(P[i][j+1]+P[i-1][j])
                        - 0.5*B_SO*(P[i][j-1]+P[i+1][j]) - 0.5*B_SW*(P[i][j-1]+P[i-1][j]);
	   	        }
        }

  /* SOR iteration */
  for(i = 1; i <= imax; i++) {
    for(j = 1; j<=jmax; j++) {
        f = flags[i][j];
        if(f%2){
            B_O = (f >> 8)&1;
            B_W = (f >> 7)&1;
            B_S = (f >> 6)&1;
            B_N = (f >> 5)&1;
            if( i==imax ) B_O = 1;
            if( i==1) B_W = 1;
            if( j==1 ) B_S = 1;
            if( j==jmax) B_N = 1;
            P[i][j] =  (1.0-omg)*P[i][j]
                    + omg/((B_O+B_W)/(dx*dx)+((B_S+B_N))/(dy*dy))*
                    ( (B_O*P[i+1][j]+B_W*P[i-1][j])/(dx*dx) + (B_N*P[i][j+1]+B_S*P[i][j-1])/(dy*dy) - RS[i][j]);

        }




    }
  }

  /* compute the residual */
  rloc = 0;
  N = 0;
  for(i = 1; i <= imax; i++) {
    for(j = 1; j <= jmax; j++) {
        f = flags[i][j];
        if(f%2){
            B_O = (f >> 8)&1;
            B_W = (f >> 7)&1;
            B_S = (f >> 6)&1;
            B_N = (f >> 5)&1;
            if( i==imax ) B_O = 1;
            if( i==1) B_W = 1;
            if( j==1 ) B_S = 1;
            if( j==jmax) B_N = 1;
            rloc += ( ( B_O*(P[i+1][j]-P[i][j]) - B_W*(P[i][j]-P[i-1][j]) )/(dx*dx) + ( B_N*(P[i][j+1]-P[i][j])-B_S*(P[i][j]-P[i][j-1]))/(dy*dy) - RS[i][j])*
                ( ( B_O*(P[i+1][j]-P[i][j]) - B_W*(P[i][j]-P[i-1][j]) )/(dx*dx) + ( B_N*(P[i][j+1]-P[i][j])-B_S*(P[i][j]-P[i][j-1]))/(dy*dy) - RS[i][j]);
            N += f%2;
        }
    }
  }
  rloc = rloc/N;
  rloc = sqrt(rloc);
  /* set residual */
  *res = rloc;






}
