#include "precice_adapter.h"
#include "adapters/c/SolverInterfaceC.h"
#include <stdlib.h>
#include <stdio.h>

void write_checkpoint(double time, double **U, double **V, double **TEMP, double *time_cp, double **U_cp, double **V_cp,
                      double **TEMP_cp, int imax, int jmax){

    //Save U backup in U_cp
    for (int i = 0; i <= imax;  i++) {
      for (int j = 0; j <= jmax+1; j++) {
        U_cp[i][j]= U[i][j];
      }
    }
    //Save V backup in V_cp
    for (int i = 0; i <= imax+1;  i++) {
      for (int j = 0; j <= jmax; j++) {
    	  V_cp[i][j]= V[i][j];
      }
    }
    //Save TEMP backup in TEMP_cp
    for (int i = 0; i <= imax+1;  i++) {
      for (int j = 0; j <= jmax+1; j++){
        TEMP_cp[i][j] = TEMP[i][j];
      }

}
}


void restore_checkpoint(double *time, double **U, double **V, double **TEMP, double time_cp, double **U_cp,
                        double **V_cp,
                        double **TEMP_cp, int imax, int jmax){

    //Restore U from U_cp
    for (int i = 0; i <= imax;  i++) {
      for (int j = 0; j <= jmax+1; j++) {
        U[i][j]= U_cp[i][j];
      }
    }
    //Restore V from V_cp
    for (int i = 0; i <= imax+1;  i++) {
      for (int j = 0; j <= jmax; j++) {
    	  V[i][j]= V_cp[i][j];
      }
    }
    //Restore TEMP from TEMP_cp
    for (int i = 0; i <= imax+1;  i++) {
      for (int j = 0; j <= jmax+1; j++){
        TEMP[i][j] = TEMP_cp[i][j];
      }
    }
}

int *precice_set_interface_vertices(int imax, int jmax, double dx, double dy, double x_origin, double y_origin,
                                        int num_coupling_cells, int meshID, int **FLAG){


	int N = precicec_getDimensions();
	double* vertices = (double*) malloc(num_coupling_cells*N*sizeof(double));
	int k = 0;

	for(int i = 0; i <= imax+1; ++i)
		for(int j = 0; j <= jmax+1; ++j){
			if( (FLAG[i][j] >> 9) & 1){				//if FLAG is set as coupling
				vertices[k] = x_origin+(i-0.5)*dx;		//set x coordinate of i-th point
				vertices[k+1] = y_origin+(j-0.5)*dy;	//set y cooridinate of i-th point
				vertices[k+2] = 0;						//set z coordinate of i-th point
				k += 3;
			}
			/* alternatively we can get rid of IF, but do a lot of overwrites */
			/*
			 	vertices[k] = x_origin+(i-0.5)*dx;		//set x coordinate of i-th point
				vertices[k+1] = y_origin+(j-0.5)*dy;	//set y cooridinate of i-th point
				veritices[k+2] = 0;						//set z coordinate of i-th point
				k += 3*(FLAG[i][j] >> 9 & 1);
			 */
	   }

	int* vertexIDs = (int*)malloc(num_coupling_cells*sizeof(int));		//TODO: free this in main
	precicec_setMeshVertices(meshID, num_coupling_cells, vertices, vertexIDs);
    
    free(vertices);
	return vertexIDs;
}

void precice_write_temperature(int imax, int jmax, int num_coupling_cells, double *temperature, int *vertexIDs,
                               int temperatureID, double **TEMP, int **FLAG)
{
	int N,S,E,W;// "bools" that are 1 when the coupling cell has a neighbour in that direction
	int k = 0;
	for(int i = 0; i <= imax +1; ++i)
		for(int j = 0; j <= jmax +1; ++j){
			if( (FLAG[i][j] >> 9) & 1){				 //if FLAG is set as coupling
				if(i>0 && i<=imax && j>0 && j<=jmax) //if we are inside the domain
				{
					N=(FLAG[i][j]>>5)&1;
					S=(FLAG[i][j]>>6)&1;
					W=(FLAG[i][j]>>7)&1;
					E=(FLAG[i][j]>>8)&1;

					//implementation is a bit hard to follow but avoids ifs
					temperature[k]=(N&E)*((TEMP[i][j+1]+TEMP[i+1][j])/2)+
								   (N&W)*((TEMP[i][j+1]+TEMP[i-1][j])/2)+
								   (S&E)*((TEMP[i][j-1]+TEMP[i+1][j])/2)+
								   (N&W)*((TEMP[i][j-1]+TEMP[i-1][j])/2);
					//at this point if no corner conditions were encountered temperature[k] is 0. Hence can be used as a "binary if"
					temperature[k]=(temperature[k]==0)*
								   N * TEMP[i][j+1] + S * TEMP[i][j-1] + E * TEMP[i+1][j] + W * TEMP[i-1][j];
				}
				else
				{
					if(i == 0) //we are on the left boundary, take TEMP From right
					{
						temperature[k] = TEMP[i+1][j];
					}
					else if(i == imax+1) // we are on the right boundary, take temp from left
					{
						temperature[k] = TEMP[i-1][j];
					}
					else if(j == 0) //we are on the bottom boundary, take TEMP From top
					{
						temperature[k] = TEMP[i][j+1];
					}
					else if(j == jmax+1) // we are on the top boundary, take TEMP from bottom
					{
						temperature[k] = TEMP[i][j-1];
					}

				}
				k++;
			}
		}
	//sanity check that temperature was populate correctly;
	if(k!=num_coupling_cells)
	{
		printf("ERR! in precice_write_temp\n");
	}

	//now write the required temperatures to preCICE
	precicec_writeBlockScalarData (
	  temperatureID,
	  num_coupling_cells,
	  vertexIDs,
	  temperature);
}

void set_coupling_boundary(int imax, int jmax, double dx, double dy, double *heatflux, double **Temp, int** flags){
    
    //********** Set boundary conditions at the coupled boundaries ********//
    int k = 0;
    double q;
    int f, B_O, B_W, B_S, B_N, B_NO, B_NW, B_SO, B_SW;
    for(int i = 0; i <= imax+1; ++i)
        for(int j = 0; j <= jmax+1; ++j){
            f = flags[i][j];
            if( (f >> 9) & 1){

                B_S = (f >> 6)&1;
                B_N = (f >> 5)&1;
                
                q = heatflux[k];
                Temp[i][j] = 0.0;
                if (j!=jmax+1) Temp[i][j] += B_N*(Temp[i][j+1] + dy*q);
                if(j!=0)       Temp[i][j] += B_S*(Temp[i][j-1] + dy*q);
                k += 1;
            }
        }
}

