
================================================ CODE USAGE ===============================================================

1. START

    - At the beginning user is asked to choose one of listed scenarios.
    - Each scenario, apart from input.dat uses parameters provided in the worksheet.
    - Customized simulation can be run, for that please modify input.dat file

   1.1. "problem" variable [IMPORTANT]
        - Each .dat file contains "problem" variable which allows user to set temperature boundary type and value as well as inflow velocities (in case of inflow scenario)
        - "problem" is structured as 10 fields delimited by colon. Colon is also put after last field.
	- Fields structure: [ N:N_v:S:S_v:W:W_v:E:E_v:U:V:], where:
        - N -   north boundary type
		- N_v - north boundary value
		- S -   south boundary type
		- S_v - south boundary value
		- W -   west boundary type
		- W_v - west boundary value
		- E -   east boundary type
		- E_v - east boundary value
		- U -   inflow velocity in x-direction
		- V -   inflow velocity in y-direction
	- Boundary types should be represented as single(!) capital "D" (Dirichlet boundary condition) or capital "N" (Neumann boundary condition).
	- Boundary values are double numbers with decimal point, e.g.: 2.55.
	- Boundary value can consist of maximum 10 characters including dot.
	- If letter different than "D" or "N" is provided boundary is assumed to be of Dirichlet type "D".
	- Example: [ N:0.0:N:0.0:D:1.0:D:0.0:0.0:0.0: ] such "problem" variable is describing Natural Convection scenario from worksheet.
	- Message informing user about grid correctness is printed to the console output.


2. RUNTIME

   2.1. Forbidden cells
	- Before any calculation, provided in .pgm file geometry is checked for forbidden cells.
	- In case of detection of forbidden cells, mesh resolution is doubled providing correct mesh.
	- Outer boundary is still kept as single "layer"

   2.2. Output folders
	- Each simulation is creating and output folder for .vtk files.
	- Output folder's name consists of chosen scenario and content of "problem" variable.
	- Output folders do NOT have to be cleaned, removed or manually created before any simulation.

================================================ OBSERVATIONS ===============================================================

1. Karman

    -As the Reynolds Number is high, we can observe a change in the direction of the original initial flow, forming an eddie after the obstacle and a general unsymmetric and unsteady flow. Running the simulation with a lower Reynolds Number (Re=1), we observe a steady flow.

2. Flow over a step
    -We observe a formation of an eddie at the immediate right side of the step, and that the original flow is widened after the step and after the eddie. Running the simulation with a lower Reynolds Number, the eddie becomes smaller and the inflow widens faster. With a higher Reynolds Number, a second eddie is formed in the higher right part of the domain.

3. Natural Convection

    3.1 Re = 1,000, t_end = 1,000
        -At the end of the simulation, we observe a "doughnut" shape for the velocity magnitude, and a more intense energy transport from the hot wall to the cold wall in the higher part of the domain.

    3.2 Re = 20,000, t_end = 10,000
        -We observe the "doughnut behavior", however in a more squared form, with velocity accumulating in the side walls and spreading in the vertical walls.

    We can explain the difference in behavior thinking of the Reynolds Number as an indicator of viscosity and thus molecular interactions within the fluid. For the lower Re, the fluid has stronger interactions with itself, and thus the loop formed is more compact and with a more symmetric form.

4. Fluid Trap

    4.1 Original Configuration
        -We see the warm fluid separated from the cold fluid (trapped), there is only a small region in between the walls where the different temperatures of the fluid meet, which prevents natural convection.

    4.2 Switched hot and cold walls
        -We observe a free convection of heat in the middle part. At the end of the simulation we observe a squared cold zone, an squared warm zone, and a Z-shaped mixed zone.


5. Rayleigh-Bénard Convection
    -At the beginning of the simulation, we see temperature changes on the whole domain, raising from the lower to the upper boundary. After a critical point in time, characteristic T-shaped forms start to appear on the whole domain.
    -For the velocity, at that critical point in time we start observing some transfer of magnitude from left to right and then back, and after that the stabilization of O-shaped velocity fields.
