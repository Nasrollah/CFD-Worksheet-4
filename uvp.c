#include "uvp.h"
#include "helper.h"
#include "boundary_val.h"

void calculate_dt(double Re, double tau, double * restrict dt, double dx, double dy, int imax, int jmax, double ** restrict U, double ** restrict V, double Pr){

	if (tau > 0){
		double umax = fabs(U[0][0]);
		double vmax = fabs(V[0][0]);

		for(int j = 0; j<=jmax+1; j++){
			for (int i = 0; i<=imax; i++){
				umax = fmax(umax,fabs(U[i][j]));
			}
		}

			for(int j = 0; j<=jmax; j++){
				for (int i = 0; i<=imax+1; i++){
					vmax = fmax(vmax,fabs(V[i][j]));
				}
		}

		double c1 = Re/( 2.0 * ( 1.0/(dx*dx) + 1.0/(dy*dy) ) );
		double c2 = dx/umax;
		double c3 = dy/vmax;
		double c4 = Pr * c1;

		if (c1<=c2 && c1<=c3 && c1<=c4) (*dt) = tau*c1;
		else if (c2<=c1 && c2<=c3 && c2<=c4) (*dt) = tau*c2;
		else if (c3<=c1 && c3<=c2 && c3<=c4) (*dt) = tau*c3;
		else
			*dt = tau*c4;
	}

}

void calculate_fg(int** flags, double Re, double GX, double GY, double alpha, double beta, double dt, double dx, double dy, int imax, int jmax, double ** restrict U, double ** restrict V, double ** restrict F, double ** restrict G, double** restrict Temp){

	double d2udx2, d2vdx2, d2udy2, d2vdy2, du2dx, dv2dy, duvdx, duvdy;

	//Compute F values
	for(int j = 1; j<=jmax; j++){
		for(int i = 1; i<=imax-1; i++){
			if( (flags[i+1][j]&1) == 1 && (flags[i][j]&1) == 1 ){ //TODO is this correct???
				//Compute the values of the discrete derivative operators
				d2udx2 = 1.0 / (dx*dx) * (U[i+1][j]-2*U[i][j]+U[i-1][j]);

				d2udy2 = 1.0 / (dy*dy) * (U[i][j+1]-2*U[i][j]+U[i][j-1]);

				du2dx  = 1.0/dx*( (U[i][j]+U[i+1][j])*(U[i][j]+U[i+1][j])/4.0 -
								  (U[i-1][j]+U[i][j])*(U[i-1][j]+U[i][j])/4.0 ) +
						 alpha/dx*(fabs(U[i][j]+U[i+1][j])*(U[i][j]-U[i+1][j])/4.0 - fabs(U[i-1][j]+U[i][j])*(U[i-1][j]-U[i][j])/4.0);

				duvdy  = 1.0/dy*( (V[i][j]+V[i+1][j])*(U[i][j]+U[i][j+1])/4.0 -
								  (V[i][j-1]+V[i+1][j-1])*(U[i][j-1]+U[i][j])/4.0) +
					 alpha/dy*(fabs(V[i][j]+V[i+1][j])*(U[i][j]-U[i][j+1])/4.0 -
					 		   fabs(V[i][j-1]+V[i+1][j-1])*(U[i][j-1]-U[i][j])/4.0);

				F[i][j] = U[i][j] + dt*( 1.0/Re*( d2udx2 + d2udy2 ) - du2dx - duvdy + GX) - 0.5*beta*dt*GX*(Temp[i][j]+Temp[i+1][j]);
			}
		}
	}

	for(int j = 1; j<=jmax-1; j++){
		for(int i = 1; i<=imax; i++){
			if( (flags[i][j+1]&1) == 1 && (flags[i][j]&1) == 1){ // TODO is this correct???
				//Compute the values of the discrete derivative operators
				d2vdx2 = 1.0 / (dx*dx) * (V[i+1][j]-2*V[i][j]+V[i-1][j]);
				d2vdy2 = 1.0 / (dy*dy) * (V[i][j+1]-2*V[i][j]+V[i][j-1]);
				dv2dy  = 1.0/dy*( (V[i][j]+V[i][j+1])*(V[i][j]+V[i][j+1])/4.0 -
				                  (V[i][j-1]+V[i][j])*(V[i][j-1]+V[i][j])/4.0 ) +
						 alpha/dy*(fabs(V[i][j]+V[i][j+1])*(V[i][j]-V[i][j+1])/4.0 -
						 fabs(V[i][j-1]+V[i][j])*(V[i][j-1]-V[i][j])/4.0);
				duvdx  = 1.0/dx*( (V[i][j]+V[i+1][j])*(U[i][j]+U[i][j+1])/4.0 -
								 (V[i-1][j]+V[i][j])*(U[i-1][j]+U[i-1][j+1])/4.0 ) +
					 alpha/dx*(fabs(U[i][j]+U[i][j+1])*(V[i][j]-V[i+1][j])/4.0 -
					 		   fabs(U[i-1][j]+U[i-1][j+1])*(V[i-1][j]-V[i][j])/4.0);

				G[i][j] = V[i][j] + dt*( 1.0/Re*( d2vdx2 + d2vdy2 ) - duvdx - dv2dy + GY) - 0.5*beta*dt*GY*(Temp[i][j]+Temp[i][j+1]);
			}

		}
	}

    //Set Boundary Values
    boundaryvalues_fg(imax, jmax, U, V, F, G, flags);

}

void calculate_rs(double dt, double dx, double dy, int imax, int jmax, double ** restrict F, double ** restrict G, double ** restrict RS){
	for(int j = 1; j < jmax + 1; ++j)
		for(int i = 1; i < imax + 1; ++i){
			RS[i][j] = (1/dt) * ( (F[i][j] - F[i-1][j])/dx + (G[i][j] - G[i][j-1])/dy );
		}
}

void calculate_uv(int** flags, double dt, double dx, double dy, int imax, int jmax, double ** restrict U, double ** restrict V, double ** restrict F, double ** restrict G, double ** restrict P){
	for(int j = 1; j < jmax + 1; ++j){
		for(int i = 1; i < imax; ++i){
			if( (flags[i+1][j]&1) == 1 && (flags[i][j]&1) == 1 ){
				U[i][j] = F[i][j] - (dt/dx) * (P[i+1][j] - P[i][j]);
			}
		}
	}

	for(int j = 1; j < jmax; ++j){
		for(int i = 1; i < imax + 1; ++i){
			if( (flags[i][j+1]&1) == 1 && (flags[i][j]&1) == 1){
				V[i][j] = G[i][j] - (dt/dy) * (P[i][j+1] - P[i][j]);
			}
		}
	}
}

void calculate_temp(double Re, double Pr, double alpha, double dt, double dx, double dy, int imax, int jmax, double** restrict U, double** restrict V, double** restrict Temp, int** restrict flags){

	double duTdx, dvTdy, d2Tdx2, d2Tdy2;
	double** newTemp = matrix ( 0 , imax+1 , 0 , jmax+1 );

	/* Compute Values for New Temperature in dummy matrix */
	for (size_t i = 1; i <= imax; i++) {
		for (size_t j = 1; j <= jmax; j++) {
			if( flags[i][j]&1 ){
				//Compute the values of the discrete derivative operators
				d2Tdx2 = 1.0 / (dx*dx) * (Temp[i+1][j]-2*Temp[i][j]+Temp[i-1][j]);

				d2Tdy2 = 1.0 / (dy*dy) * (Temp[i][j+1]-2*Temp[i][j]+Temp[i][j-1]);

				duTdx  = 0.5/dx*( U[i][j]*(Temp[i][j]+Temp[i+1][j]) - U[i-1][j]*(Temp[i-1][j]+Temp[i][j])) +
					 0.5*alpha/dx*(fabs(U[i][j])*(Temp[i][j]-Temp[i+1][j]) - fabs(U[i-1][j])*(Temp[i-1][j]-Temp[i][j]));

				dvTdy  = 0.5/dy*( V[i][j]*(Temp[i][j]+Temp[i][j+1]) - V[i][j-1]*(Temp[i][j-1]+Temp[i][j]) ) +
					 0.5*alpha/dy*(fabs(V[i][j])*(Temp[i][j]-Temp[i][j+1]) - fabs(V[i][j-1])*(Temp[i][j-1]-Temp[i][j]));

				newTemp[i][j] = Temp[i][j] + dt*( 1.0/(Re*Pr)*(d2Tdx2 + d2Tdy2) - duTdx - dvTdy );
			}
		}
	}
	/* Copy computed values to Temp matrix */
	for (size_t i = 1; i <= imax; i++) {
		for (size_t j = 1; j <= jmax; j++) {
			if( flags[i][j]&1 ){
				Temp[i][j] = newTemp[i][j];
			}
		}
	}

	free_matrix ( newTemp, 0, imax+1, 0 , jmax+1);

}
