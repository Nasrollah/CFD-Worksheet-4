#include "helper.h"
#include "init.h"

int read_parameters( const char *szFileName,   /* name of the file */
                    double *Re,                /* reynolds number   */
                    double *UI,                /* velocity x-direction */
                    double *VI,                /* velocity y-direction */
                    double *PI,                /* pressure */
                    double *GX,                /* gravitation x-direction */
                    double *GY,                /* gravitation y-direction */
                    double *t_end,             /* end time */
                    double *xlength,           /* length of the domain x-dir.*/
                    double *ylength,           /* length of the domain y-dir.*/
                    double *dt,                /* time step */
                    double *dx,                /* length of a cell x-dir. */
                    double *dy,                /* length of a cell y-dir. */
                    int  *imax,                /* number of cells x-direction*/
                    int  *jmax,                /* number of cells y-direction*/
                    double *alpha,             /* uppwind differencing factor*/
                    double *omg,               /* relaxation factor */
                    double *tau,               /* safety factor for time step*/
                    int  *itermax,             /* max. number of iterations  */
		                                       /* for pressure per time step */
                    double *eps,               /* accuracy bound for pressure*/
					double *dt_value,		   /* time for output */
					char* geometry,	           /* name of pgm file */
					char* problem,	           /* additional parameteres */
					double *TI,
					double *Pr,
                    double *beta,
                    double *x_origin,
                    double *y_origin,
                    char *precice_config,
                    char *participant_name,
                    char *mesh_name,
                    char *read_data_name,
                    char *write_data_name)
{
   READ_DOUBLE( szFileName, *xlength );
   READ_DOUBLE( szFileName, *ylength );

   READ_DOUBLE( szFileName, *Re    );
   READ_DOUBLE( szFileName, *t_end );
   READ_DOUBLE( szFileName, *dt    );

   READ_INT   ( szFileName, *imax );
   READ_INT   ( szFileName, *jmax );

   READ_DOUBLE( szFileName, *omg   );
   READ_DOUBLE( szFileName, *eps   );
   READ_DOUBLE( szFileName, *tau   );
   READ_DOUBLE( szFileName, *alpha );

   READ_INT   ( szFileName, *itermax );
   READ_DOUBLE( szFileName, *dt_value );

   READ_DOUBLE( szFileName, *UI );
   READ_DOUBLE( szFileName, *VI );
   READ_DOUBLE( szFileName, *GX );
   READ_DOUBLE( szFileName, *GY );
   READ_DOUBLE( szFileName, *PI );

   *dx = *xlength / (double)(*imax);
   *dy = *ylength / (double)(*jmax);

   READ_STRING( szFileName, geometry);
   READ_STRING( szFileName, problem);

   READ_DOUBLE( szFileName, *TI);
   READ_DOUBLE( szFileName, *Pr);
   READ_DOUBLE( szFileName, *beta);
   
   READ_DOUBLE( szFileName, *x_origin);
   READ_DOUBLE( szFileName, *y_origin);
   
   READ_STRING( szFileName, precice_config);
   READ_STRING( szFileName, participant_name);
   READ_STRING( szFileName, mesh_name);
   READ_STRING( szFileName, read_data_name);
   READ_STRING( szFileName, write_data_name);
   
   return 1;
}

void init_uvp( double UI, double VI, double PI, double TI, int imax, int jmax, double **U, double **V, double **P, int **flags, double** Temp)
{
    //Initialize U
    for (size_t i = 0; i <= imax;  i++) {
      for (size_t j = 0; j <= jmax+1; j++) {
        U[i][j] = (double)(flags[i][j]&1) * UI;
      }
    }
    //Initialize V
    for (size_t i = 0; i <= imax+1;  i++) {
      for (size_t j = 0; j <= jmax; j++) {
        V[i][j] = (double)(flags[i][j]&1) * VI;
      }
    }
    //Initialize P
    for (size_t i = 0; i <= imax+1;  i++) {
      for (size_t j = 0; j <= jmax+1; j++){
        P[i][j] = (double)(flags[i][j]&1) * PI;
      }
    }
    //Initialize T
    for (size_t i = 0; i <= imax+1;  i++) {
      for (size_t j = 0; j <= jmax+1; j++){
        Temp[i][j] = TI;
      }
    }
}

int** init_flags_and_count_coupling(int xsize, int ysize, int** pic, int *coupling_count){

	*coupling_count = 0;
	int**flags;
	flags = (int**)malloc(sizeof(int*) * (xsize));
	for(int i = 0; i < xsize; ++i){
		flags[i] = (int *) malloc(sizeof(int) * (ysize));
		for(int j = 0; j < ysize; ++j){
			switch(pic[i][j])
			{
			case 0: //no slip
				flags[i][j]=0x02; //b' 00 0000 0010
				break;
			case 1: //free slip
				flags[i][j]=0x04; //b' 00 0000 0100
				break;
			case 2: //outflow
				flags[i][j]=0x08; //b' 00 0000 1000
				break;
			case 3: //inflow
				flags[i][j]=0x10; //b' 00 0001 0000
				break;
			case 4: //coupling in terms of temperature, and also no slip in terms of velocities!
				flags[i][j]=0x202; //b' 10 0000 0010
				(*coupling_count)++;
				break;
			case 6: //fluid
				flags[i][j]=0x01; //b' 00 0000 0001
				break;
			default:
				printf("ERROR! Invalid geometry image used as input!\n");
			}

		}
	}


	return flags;
}
/* this is old code for assignment 2, now everything is done in init_flags_and_count_coupling*/
#if deprecated
void set_cell_type(int** flags, int** pic, int* xsize, int* ysize){

	int temp;
	for(int i = 0; i < *xsize; ++i){
		for(int j = 0; j < *ysize; ++j){
			if(pic[i][j] !=)
			{
				temp = (pic[i][j] + 1) % 5;
				flags[i][j] = (flags[i][j] << temp);
			}
			else
			{

			}
		}
	}
}
#endif


void set_cell_neighbours(int** flags, int xsize, int ysize){

	/* south points check and set */
	for(int i = 1; i < xsize-1; ++i){
		flags[i][0] = flags[i][0] | (flags[i][1]%2 << 5) | (flags[i-1][0]%2 << 7) | (flags[i+1][0]%2 << 8);
	}

	/* north points check and set */
	for(int i = 1; i < xsize-1; ++i){
		flags[i][ysize-1] = flags[i][ysize-1] | (flags[i][ysize-2]%2 << 6) | (flags[i-1][ysize-1]%2 << 7) | (flags[i+1][ysize-1]%2 << 8);
	}

	/* east points check and set */
	for(int j = 1; j < ysize-1; ++j){
		flags[xsize-1][j] = flags[xsize-1][j] | (flags[xsize-1][j+1]%2 << 5) | (flags[xsize-1][j-1]%2 << 6) | (flags[xsize-2][j]%2 << 8);
	}

	/* west points check and set */
	for(int j = 1; j < ysize-1; ++j){
		flags[0][j] = flags[0][j] | (flags[0][j+1]%2 << 5) | (flags[0][j-1]%2 << 6) | (flags[1][j]%2 << 7);
	}

	/* corner west south */
	flags[0][0] = flags[0][0] | (flags[0][1]%2 << 5) | (flags[1][0]%2 << 7);

	/* corner west north */
	flags[0][ysize-1] = flags[0][ysize-1] | (flags[0][ysize-2]%2 << 6) | (flags[1][ysize-1]%2 << 7);

	/* corner south east */
	flags[xsize-1][0] = flags[xsize-1][0] | (flags[xsize-1][1]%2 << 5) | (flags[xsize-2][0]%2 << 8);

	/* corner south west */
	flags[xsize-1][ysize-1] = flags[xsize-1][ysize-1] | (flags[xsize-1][ysize-2]%2 << 6) | (flags[xsize-2][ysize-1]%2 << 8);

	/* inner points check and set */
	for(int i = 1; i < xsize-1; ++i){
		for(int j = 1; j < ysize-1; ++j){
			flags[i][j] = flags[i][j] | (flags[i][j+1]%2 << 5) | (flags[i][j-1]%2 << 6) | (flags[i-1][j]%2 << 7) | (flags[i+1][j]%2 << 8);
		}
	}
}

void check_grid(int** pic, int** flags, int xsize, int ysize, int * coupling_count){

	int B_O, B_W, B_S, B_N;
	for(int i = 0; i < xsize; ++i){
		for(int j = 0; j < ysize; ++j){
			if( (flags[i][j]&1) == 0){

				B_O = (flags[i][j] >> 8) & 1;
				B_W = (flags[i][j] >> 7) & 1;
				B_S = (flags[i][j] >> 6) & 1;
				B_N = (flags[i][j] >> 5) & 1;

				if ((B_O & B_W) | (B_S & B_N)){
					printf("Error in grid!\n");
					printf("Automatic grid refinement deprecated. You should just provide correct, scaled geometry input!\n");
					return;
#if deprecated
					int **flags2;
					*xsize = *xsize * 2;
					*ysize = *ysize * 2;
					flags2 =  init_flags_and_count_coupling(xsize, ysize, pic,coupling_count);
					populate_new_flags(flags, flags2, xsize, ysize);
					free_flags(flags2, *xsize);
					return;
#endif
				}
			}
		}
	}
	printf("Grid is correct\n");
}

//NOTE: function deprecated as it doesn't count the coupling count, and calls some deprecated functions
void populate_new_flags(int** flags, int** new_flags, int* new_xsize, int* new_ysize){

	for(int i = 0; i < *new_xsize; i += 2){
		for(int j = 0; j < *new_ysize; j += 2){
			new_flags[i][j] = flags[i/2][j/2];
			new_flags[i+1][j] = flags[i/2][j/2];
			new_flags[i][j+1] = flags[i/2][j/2];
			new_flags[i+1][j+1] = flags[i/2][j/2];
		}
	}
	int x = *new_xsize - 2;
	int y = *new_ysize - 2;
	int** tmp = NULL ; //init_flags(&x,&y);
	printf("new x: %d, new y: %d, x: %d, y: %d \n",*new_xsize, *new_ysize, x, y);
	for(int i = 0; i < x; ++i){
		for(int j = 0; j < y; ++j){
			tmp[i][j] = new_flags[i+1][j+1];
		}
	}

	for(int i = 0; i < x; ++i){
		for(int j = 0; j < y; ++j){
			printf("%d", tmp[i][j]&1);
		}
		printf("\n");
	}
	*new_xsize = x;
	*new_ysize = y;

	free(flags);
	flags= tmp;

}

void free_flags(int** flags, int xsize){

	for (int i = 0; i < xsize; i++){
	    free(flags[i]);
	}
	free(flags);
	flags = NULL; //TODO check this
}

void choose_problem(char* FileName, int argn, char** args){
	int s=0;
    
    printf("================================================================\n");
	printf("AVAILABLE SCENARIOS:\n");
	printf("1 -	Heated plate\n");
	printf("2 -	Convection\n");
	printf("3 -	Heat Exchange 1\n");
	printf("4 -	Heat Exchange 2\n");
	printf("================================================================\n\n");
	printf("Select simulation scenario (number from 1 to 4):	");
    
    FileName[0] = 0x00;
    strcat(FileName,"configs/");
    
    if (argn==1) scanf("%d",&s);
    else sscanf(args[1],"%d",&s);
    switch (s)
    {
        case 1:
            strcat(FileName, "heated-plate.dat");
            break;
        case 2:
            strcat(FileName, "convection.dat");
            break;
        case 3:
            strcat(FileName, "F1-heat-exchange.dat");
            break;
        case 4:
            strcat(FileName, "F2-heat-exchange.dat");
            break;
        default:
            printf("Invalid Scenario. Running Heated Plate Scenario.\n");
            strcat(FileName, "heated-plate.dat");
            break;
    }

}
